module.exports = ()=>{
    return {
        getTotalPrice : (price , quantity)=>{
            return (Number(price) * Number(quantity));
        }
    }
};