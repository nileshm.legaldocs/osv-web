module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.get('/food-items',async (req,res)=>{
        let category = (req.query.category)?req.query.category:'snacks';
        let getFoodData = (await pool.queryAsync(`SELECT * FROM food_items Where category='${category}' AND quantity > 0 `));
        let subMenuData = (await pool.queryAsync(`SELECT DISTINCT(category) from food_items`));

        let user = '',name = '',phone='';
        if(req.session.user && req.session.user.auth){
            user = true,
            name = req.session.user.name,
            phone = req.session.user.phone;
        }

        res.render('food-items',({
            title:'Food Items',
            asyncFn:{},
            foodItem :(getFoodData)?getFoodData:'',
            subMenu : subMenuData,
            login : user,
            user:{
                "name":name,
                "phone":phone
            }
        }));
    });

    router.post('/food-items',async (req,res)=>{
        let requestedCategory = req.body.category;
        if(requestedCategory){
           let getFoodData = (await pool.queryAsync(`SELECT * FROM food_items Where category='${requestedCategory}' AND quantity > 0 `));
           (getFoodData != '[]')?res.send({'status':'success',data:getFoodData}):res.send({'staus':'success','msg':`we didn't found any food itme on this category`})
        }else{
            res.send({'status':'err','msg':'request category is empty'})
        }
    });
}