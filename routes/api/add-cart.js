module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.post('/add-cart',async (req,res) => {
        let quantity = req.body.quantity;
        let product = req.body.product;
        let plates = (req.body.plates)?req.body.plates:'';

        let user = req.session.user;
        if(!user){
            return res.send({"status":"err","code":6,"msg":"login required"});
        }
        
        if(quantity && product){
            let getProductData = (await pool.queryAsync(`SELECT * FROM food_items WHERE pid = ${product}`))[0];
            let pid = getProductData['pid']; 

            if(getProductData['quantity'] >= quantity ){
                let newQuantity =  getProductData['quantity'] - quantity; 
                let updateQuantity = (await pool.queryAsync(`UPDATE food_items SET quantity = ${newQuantity} WHERE pid=${product}`));
                
                if(updateQuantity.affectedRows){
                    let getCartStatus = (await pool.queryAsync(`SELECT * FROM cart WHERE uid = ${req.session.user.uid} AND order_time = 0`))[0];
                    
                    if(getCartStatus.cid){
                        let addedProduct = JSON.parse(getCartStatus['items']);
                        for(let i = 0 ; i < addedProduct.length ; i++ ){
                            let productObj = addedProduct[i];
                            if(productObj['pid']==product){
                                addedProduct.splice(i,1);
                            }
                        }
                        addedProduct.push({
                            'pid':pid,
                            'quantity':Number(quantity)
                        });  
                        
                        addedProduct = JSON.stringify(addedProduct);

                        let updateCart = (await pool.queryAsync(`UPDATE cart SET items = '${addedProduct}'`));
                        if(updateCart.affectedRows){
                            res.send({'status':'success','code':4,'msg':'Added to cart'});
                        }else{
                            // if unable to add cart in db then it will trigger.
                            (await pool.queryAsync(`UPDATE food_items SET quantity = ${getProductData['quantity']} WHERE pid=${product}`));
                            res.send({'status':'err','code':5,'msg':'something went wrong please try again'});
                        }
                    }else{
                        let itemsArray = [];
                        let itemObj = {};
                        
                        itemObj['pid']=pid;
                        itemObj['quantity']=Number(quantity);
                        itemObj['plate']=plates;
                        
                        itemObj = JSON.stringify(itemObj);
                        itemsArray.push(itemObj);

                        let insertCart = (await pool.queryAsync(`INSERT INTO cart (uid,phone,items,timestamp,status) VALUES (${user.uid},${user.phone},' [ ${itemsArray} ]','${+new Date()}','active')`));
                        if(insertCart.insertId){    
                            res.send({'status':'success','code':4,'msg':'Added to cart'});
                        }else{
                            // if unable to add cart in db then it will trigger.
                            (await pool.queryAsync(`UPDATE food_items SET quantity = ${getProductData['quantity']} WHERE pid=${product}`));
                            res.send({'status':'err','code':5,'msg':'something went wrong please try again'});
                        }
                    }
                }else{
                    res.send({'status':'err','code':3,'msg':'oops we left with '+getProductData['quantity']+' quantity'});

                }
            }else{
                res.send({'status':'err','code':2,'msg':'oops we left with '+getProductData['quantity']+' quantity'});
            }
        }else{
            res.send({'status':'err','code':1,'msg':'product or quantity something is missing'});
        }
    });
}