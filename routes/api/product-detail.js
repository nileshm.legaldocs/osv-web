module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.post('/product-detail',async (req,res)=>{
        const pid = req.body.pid;
        let uid = (req.session.user)?req.session.user.uid:'';
        
        let getProduct = (await pool.queryAsync(`SELECT * FROM food_items WHERE pid = ${pid}`))[0];
        if(uid){
            let getCart = (await pool.queryAsync(`SELECT * FROM cart WHERE uid = ${uid} AND status = 'active' AND order_time = 0`))[0];
            let getproductCart = (getCart['items'])?JSON.parse(getCart['items']):'';
            let quantityInCart = 0, quantitySize = '-';
            if(getproductCart){
                for(let i = 0 ; i < getproductCart.length; i++ ){
                    let productObj = getproductCart[i];
                    if(productObj['pid']==pid){
                        quantityInCart += productObj['quantity'];
                        quantitySize = (productObj['plate'])?productObj['plate']:'F';
                    }                
                }
            }
            getProduct['cart'] = quantityInCart;
            getProduct['size'] = quantitySize;
        }

        if(getProduct.pid){
          res.send({'status':'success','product':getProduct});  
        }else{
            res.send({'status':'err','msg':'oops we have sold out '+getProduct.name});
        }
    });
}