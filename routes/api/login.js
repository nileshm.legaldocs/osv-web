var Hash = require('../../models/hash');
var hash = new Hash();

module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    // var passwordHash=hash.md5(pass).toString();
    // var authKey=hash.md5(phone+'|'+passwordHash).toString();
    
    router.post('/get-otp',async (req,res)=>{
        let phone = req.body.phone;
        if(phone){
            let getPhoneData = (await pool.queryAsync(`SELECT * FROM user WHERE phone = ${phone}`))[0];
            if(!getPhoneData.uid){
                let otp = Math.floor(1000 + Math.random() * 9000);
                let insertPhone = (await pool.queryAsync(`INSERT INTO user (phone,otp,timestamp) VALUES (${phone},${otp},${+new Date()})`));
                if(insertPhone.insertId){
                    res.send({'status':"success","code":1});
                }else{
                    res.send({'status':"err","code":2,"msg":"<p> oops something went wrong at our side please try again. </p>"});
                }
            }else if(!getPhoneData.registration){
                res.send({'status':"err","code":5,"msg":"<p> Please do Register to proceed </p>"});
            }else if(getPhoneData.timestamp){
                res.send({"status":"err","code":3,"msg":"<p> You are already Register with us please do Login. </p>"});
            }
        }else{
            res.send({"status":"err","code":4,"msg":"<p> oops something went wrong please try again.</p>"});
        }
    });

    router.post('/register', async (req,res)=>{
        let name = req.body.name,
            phone = req.body.phone,
            otp = req.body.otp,
            pass = req.body.password;

            if(name && phone && otp && pass){
                let getOtp = (await pool.queryAsync(`SELECT otp,auth_key FROM user WHERE phone = ${phone}`))[0];

                if(getOtp.otp == otp && !getOtp.auth_key){
                    let passHash = hash.md5(pass),
                        authKey=hash.md5(passHash+""+phone);

                    let insertReg = (await pool.queryAsync(`UPDATE user SET auth_key = '${authKey}', name = '${name}' , password = '${pass}', password_hash='${passHash}', registration=${+new Date()} WHERE phone = ${phone}`))
                    let getUser = (await pool.queryAsync(`SELECT * FROM user WHERE phone=${phone}`))[0];
                    if(insertReg.affectedRows){
                        req.session.user = {
                            "uid":getUser['uid'],
                            "name":name,
                            "phone":phone,
                            "auth":authKey,
                        };
                        req.session.cookie.expires = (+new Date() + 31536e7);
                    }
                    delete req.session.loginOtp;
                    res.send({'status':'success'});

                }else if(getOtp.auth_key){
                    res.send({"status":"err","code":3,"msg":"Already Register user"})

                }else {
                    res.send({"status":"err","code":2,"msg":"Otp doesnt Match"})
                }

            }else{
                res.send({"status":"err","code":1,"msg":"something is empty"});
            }
    });

    router.post('/resend-otp',async (req,res)=>{
        let phone = req.body.phone;
        let getRequestedOtp = (await pool.queryAsync(`SELECT otp FROM user WHERE phone = ${phone}`))[0];
        if(getRequestedOtp.otp){
            console.log(getRequestedOtp.otp); //OTP SEND
            res.send({"status":"success","code":2})
        }else{
            res.send({"status":"err","code":1,"msg":"oops something went wrong"});
        }
    });

    router.post('/login-password',async (req,res)=>{
        let phone = req.body.phone;
        let password = req.body.password;

        if(phone && password){
            let passHash = hash.md5(password);
            let checkAuth = (await pool.queryAsync(`SELECT auth_key FROM user WHERE phone=${phone}`))[0];
            if(checkAuth.auth_key){
                let getLogin = (await pool.queryAsync(`SELECT * FROM user WHERE phone = ${phone} AND password_hash = '${passHash}'`))[0];
                if(getLogin.auth_key){
                    req.session.user={
                        "uid":getLogin["uid"],
                        "name":getLogin['name'],
                        "phone":getLogin['phone'],
                        "auth":getLogin['auth_key'],
                    }
                    res.send({"status":"success"})
                }else{
                    res.send({"status":"err","code":3,"msg":"password mismatch"});
                }
            }else{
                res.send({"status":"err","code":2,"msg":"Please do Registration First"});
            }
        }else{
            res.send({"status":"err","code":1,"msg":"oops something is missing."})
        }
    });

    router.post('/send-otp',async (req,res)=>{
        let phone = req.body.phone;
        var patternPhone=/^\d{10}$/;
        
        if(patternPhone.test(phone)){
            let checUser = (await pool.queryAsync(`SELECT auth_key FROM user WHERE phone = ${phone}`))[0];

            if(checUser.auth_key){
                if(req.session.loginOtp){
                    console.log(req.session);
                }else{
                    let otp = Math.floor(1000 + Math.random() * 9000);
                    req.session['loginOtp'] = otp;
                    console.log(req.session);
                }

                res.send({"status":"success","code":3,"msg":"<p> OTP Sent </p>"})

            }else{
                res.send({"status":"err","code":1,"msg":"<p> Registration is Required to Login </p>"})
            }
        }else{
            res.send({"status":"err","code":2,"msg":"<p> Phone number is missing </p>"})
        }
    });

    router.post('/verify-otp',async (req,res)=>{
        let otp = req.body.otp;
        let phone = req.body.phone;

        if(otp && phone && otp.toString().length == 4 ){
            let sessionOtp = req.session.loginOtp
            if(sessionOtp == otp){
                let getUser = (await pool.queryAsync(`SELECT * FROM user WHERE phone=${phone}`))[0];
                delete req.session.loginOtp;
                req.session.user = {
                    "uid":getUser["uid"],
                    "name":getUser['name'],
                    "phone":getUser['phone'],
                    "auth":getUser['auth_key'],
                };
                req.session.cookie.expires = (+new Date() + 31536e7);

                res.send({"status":"success"});
            }else{
                res.send({"status":"err","code":2,"msg":"OTP Mismatch"});
            }
        }else{  
            res.send({"status":"err","code":1,"msg":"OTP is missing"});
        }
    });

}