
module.exports = (imp) => {
    var router = imp.router;
    
    router.get('/logout',async (req,res)=>{
        delete req.session.user;
        res.redirect('/home');
    });
}