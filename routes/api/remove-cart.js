module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.post('/remove-product-from-cart', async (req,res) => {
        let pid = req.body.pid;
        let user = req.session.user;

        if(pid && user.uid){
            let getCart = (await pool.queryAsync(`SELECT * FROM cart WHERE uid = ${user.uid} AND status = 'active' AND order_time = 0`))[0];
            
            if(getCart.items){
                let cartItems = JSON.parse(getCart.items); 
                let product ='', quantity = '';
                for(let i = 0; i < cartItems.length; i++){
                    let selectedProduct = cartItems[i].pid;
                    if(selectedProduct == pid){
                        product = cartItems[i].pid;
                        quantity = cartItems[i].quantity;
                        cartItems.splice(i,1);
                    }
                }
                cartItems = JSON.stringify(cartItems); 
                let updateCartNow = (await pool.queryAsync(`UPDATE cart SET items = '${cartItems}' WHERE  uid = ${user.uid} AND status = 'active' AND order_time = 0`));
                if(updateCartNow.affectedRows){
                    let getQuantity = (await pool.queryAsync(`SELECT quantity FROM food_items WHERE pid = ${pid}`))[0];
                    let updateQuantity = Number(getQuantity.quantity) + Number(quantity);
                    (await pool.queryAsync(`UPDATE food_items SET quantity = ${updateQuantity} WHERE pid = ${pid}`));
                    res.send({"status":"success"});
                }else{
                    res.send({'status':'err','code':3,'msg':'oops something went wrong'});
                }
            }else{ 
                res.send({'status':'err','code':2,'msg':'cart not found'});
            }
        }else{
            res.send({'status':'err','code':1,'msg':'user session or pid is missing'});
        }
    });
}