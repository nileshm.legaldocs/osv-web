module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.get('/home',async (req,res)=>{
        var getActiveProduct = (await pool.queryAsync(`SELECT * FROM food_items WHERE quantity >= 1 ORDER BY pid ASC LIMIT 6`));
        let user = '',name = '',phone='';

        if(req.session.user && req.session.user.auth){
            user = true,
            name = req.session.user.name,
            phone = req.session.user.phone;
        }

        res.render('index',{
            title:'om sai vada paw, koparkhairane',
            product:getActiveProduct,
            login : user,
            user:{
                "name":name,
                "phone":phone
            }
        });
    });

}