module.exports = (imp) => {
    var router = imp.router;
    var pool = imp.pool;

    router.get('/checkout',async (req,res) => {
        let user = req.session.user;
        let name = req.session.user.name;
        let phone = req.session.user.phone;
        if(user){            
            let cartData = (await pool.queryAsync(`SELECT * FROM cart WHERE uid = ${user.uid} AND order_time =0`))[0];
            if(cartData.cid && cartData.items){
                let foodItem = JSON.parse(cartData.items);
                let itemArray = new Array();
                for(let i = 0 ; i < foodItem.length; i++ ){
                    let productData = (await pool.queryAsync(`SELECT name,price,category,img_location FROM food_items WHERE pid = ${foodItem[i].pid}`))[0];
                         
                    foodItem[i]['index'] = i+1;
                    foodItem[i]['pid']= foodItem[i].pid;
                    foodItem[i]['name'] = productData['name'];
                    foodItem[i]['price'] = productData['price'];
                    foodItem[i]['category'] = productData['category'];
                    foodItem[i]['img_location'] = productData['img_location'];                    
                    itemArray.push(foodItem[i]);
                }
                res.render('checkout',{
                            title:'Order Checkout',
                            foodItem : itemArray,
                            login : user,
                            user:{
                                "name":name,
                                "phone":phone
                            }
                        });
            }else{
                res.send({"status":"err","code":2,"msg":"cart is empty"});
            }
  
        }else{
            res.send({"status":"err","code":1,"msg":"Login is required"});
        }
    });
}