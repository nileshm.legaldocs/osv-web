var express = require('express');
var app = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');
var session = require('express-session')

var env = require('./env');
var sqlSession = require('express-mysql-session')(session);
var sessionStore = new sqlSession({
    host: env.db.host,
    port: env.db.port,
    user: env.db.user,
    password: env.db.password,
    database: env.db.database
})
/**
 * USER DEFINE MODEL
 * pool model to get the DB data from mysql
 */
var Pool = require('./models/pool');
var pool = new Pool(env.db.host,env.db.user,env.db.password,env.db.database);

/* Template Engine set */
app.engine('hbs', expressHbs({
    helpers: require('./models/helpers')(),
    extname:'hbs',
    defaultLayout:'layout',
    layoutsDir:__dirname+'/views/layouts/',
    partialsDir:__dirname+'/views/partials/'
}));
app.set('view engine','hbs');

/* Static Directory for static files storages */
app.use(express.static('public'));

/**
 * SESSION STORAGE
 */
app.use(session({secret:'anySecret',store: sessionStore, saveUninitialized: true,resave: true}));

/**
 * Body Parse use for the req.body to get the body data for post Request
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

/**
 * Logger is used to show log in console of routes
 */
app.use(logger('dev'));

app.get('/', async (req,res)=>{
    res.redirect('/home');
});

app.use('/',require('./routes/webPages'));
app.use('/api',require('./routes/api/_router'));

let port = env.port;
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));